import React, { Component } from 'react';
import { Router } from 'react-router-dom';
import history from './services/history';
import './App.css';
import Signin from './signin/signin.js'
import Routes from './routes';
//import Dashboard from ./dashboard/dashboard.js
class App extends Component {
  render() {
    return (
      <Router history={history}>
      <Routes />
    </Router>
    );
  }
}
export default App;
