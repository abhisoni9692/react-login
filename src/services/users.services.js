
export const fetchUsers = {fetchUsers};

fetchUsers() {
  // Where we're fetching data from
  fetch(`../json.dummyjson.json`)
    // We get the API response and receive data in JSON format...
    .then(response => response.json())
    // ...then we update the users state
    .then(data =>
      this.setState({
        users: data,
        isLoading: false,
      })
    )
    // Catch any errors we hit and update the app
    .catch(error => this.setState({ error, isLoading: false }));
}
