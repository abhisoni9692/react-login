import React from 'react';
import data from "../json/dummyjson"; 
const socialMediaList = data.WelcomeNote;

function Dashbaord() {
  return (
    <div className="App">
      <header className="App-header">
        <span>{socialMediaList}</span>   
      </header>
    </div>
  );
}

export default Dashbaord;
