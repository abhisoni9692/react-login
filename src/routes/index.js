
import React from 'react';
import { Switch} from 'react-router-dom';
import Route from './Route';
import Signin from '../signin/signin';
import Dashboard from '../dashboard/dashboard';

export default function Routes() {
  return (
    <Switch>
      <Route path="/" exact component={Signin} />
      <Route path="/dashboard" component={Dashboard} />
    </Switch>
  );
}		