import React from 'react';
import { Router, Route, Redirect } from 'react-router-dom';

import './signin.css';
import data from "../json/dummyjson"; 
import Dashboard  from '../dashboard/dashboard'
//import { userService } from '../services/users.services.js';


class Signin extends React.Component {

  constructor(props){
   super(props);
   this.state = { username: "admin@gmail.com", 
   password : "123456", 
   items : [],
   test : "cscs",
   isLoaded : false
 };
   this.onSubmitClick = this.onSubmitClick.bind(this);
 }


  handleChangeid= (event)=> {
    this.setState({
      username: event.target.value
    });
  }
  handleChangPassword= (event)=> {
    this.setState({
      password: event.target.value
    });
  }
  componentDidMount(){
   fetch(`https://s3-us-west-2.amazonaws.com/s.cdpn.io/3/posts.json`)
      // We get a response and receive the data in JSON format...
      .then(response => response.json())
      // ...then we update the state of our application
      .then(
        data =>
          this.setState({
            items: data,
            isLoading: false,
          })
      )
      // If we catch errors instead of a response, let's update the app
      .catch(error => this.setState({ error, isLoading: false }));
  }
  onSubmitClick = () => {
    console.log(this.state.items)
      if(!this.state.username || !this.state.password){
        alert("LoginId and Password is compulsory")
      }else{
        if(this.state.username == 'admin@gmail.com' && this.state.password =="123456"){
       // console.log("dsdas",this.state.items)
           window.location.href="dashboard";
        }else{
          alert("Incorrect LoginId or Password")
       }
    }    
  }

  render(){
    return (
    <div className="App">
      <header className="App-header">
        <div className="Signin-main-div">
          <span className="signin-header-style">Signin</span>
          <div className="login-div">
            <span className="login-span">LoginId</span>
            <input className="login-input" id="email" type="text"  value={this.state.username} onChange={this.handleChangeid}
              pattern="[^]+@[^]+[.][a-z]{2,63}$" autoComplete="email" required></input>
          </div>
          <div className="password-div">
            <span className="password-span">Password</span>
            <input className="password-input" id="password" type="password" value={this.state.password}
            value={this.state.password} onChange={this.handleChangPassword} name="password" required></input>
          </div>
          <div className="submit-div">
              <button onClick={this.onSubmitClick} className="submit-button">Submit</button>
          </div>

        </div>       
      </header>
    </div>
    );
  }
}
export default Signin;
